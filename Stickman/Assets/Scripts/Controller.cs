﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	public float speed;
	public float jumpforce;
	private Rigidbody2D rgbody;
	private bool canJump;
	// Use this for initialization
	void Start () {
		rgbody = GetComponent<Rigidbody2D> ();
		canJump = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void FixedUpdate() {
		float translation = Input.GetAxis ("Horizontal") * speed;
		float jump = 0;
		if (canJump) {
			jump = Input.GetAxisRaw ("Vertical") * jumpforce;
		}
		//jump *= Time.deltaTime;
		//translation *= Time.deltaTime;
		//transform.Translate (translation, 0, 0);
		//transform.Translate (0, jump, 0);
		Vector2 forceVec = new Vector2 (translation, jump);
		rgbody.AddForce (forceVec);
	}

	void OnCollisionEnter2D(Collision2D other){
		canJump = true;
	}

	void OnCollisionExit2D(Collision2D other){
		canJump = false;
	}

}
